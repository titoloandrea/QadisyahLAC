<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.22.16-Białowieża">
  <identifier></identifier>
  <parentidentifier></parentidentifier>
  <language></language>
  <type>dataset</type>
  <title></title>
  <abstract></abstract>
  <contact>
    <name></name>
    <organization></organization>
    <position></position>
    <voice></voice>
    <fax></fax>
    <email></email>
    <role></role>
  </contact>
  <links/>
  <fees></fees>
  <encoding></encoding>
  <crs>
    <spatialrefsys>
      <wkt>PROJCRS["WGS 84 / UTM zone 37N",BASEGEOGCRS["WGS 84",DATUM["World Geodetic System 1984",ELLIPSOID["WGS 84",6378137,298.257223563,LENGTHUNIT["metre",1]]],PRIMEM["Greenwich",0,ANGLEUNIT["degree",0.0174532925199433]],ID["EPSG",4326]],CONVERSION["UTM zone 37N",METHOD["Transverse Mercator",ID["EPSG",9807]],PARAMETER["Latitude of natural origin",0,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8801]],PARAMETER["Longitude of natural origin",39,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8802]],PARAMETER["Scale factor at natural origin",0.9996,SCALEUNIT["unity",1],ID["EPSG",8805]],PARAMETER["False easting",500000,LENGTHUNIT["metre",1],ID["EPSG",8806]],PARAMETER["False northing",0,LENGTHUNIT["metre",1],ID["EPSG",8807]]],CS[Cartesian,2],AXIS["(E)",east,ORDER[1],LENGTHUNIT["metre",1]],AXIS["(N)",north,ORDER[2],LENGTHUNIT["metre",1]],USAGE[SCOPE["unknown"],AREA["World - N hemisphere - 36°E to 42°E - by country"],BBOX[0,36,84,42]],ID["EPSG",32637]]</wkt>
      <proj4>+proj=utm +zone=37 +datum=WGS84 +units=m +no_defs</proj4>
      <srsid>3121</srsid>
      <srid>32637</srid>
      <authid>EPSG:32637</authid>
      <description>WGS 84 / UTM zone 37N</description>
      <projectionacronym>utm</projectionacronym>
      <ellipsoidacronym>EPSG:7030</ellipsoidacronym>
      <geographicflag>false</geographicflag>
    </spatialrefsys>
  </crs>
  <extent>
    <spatial crs="EPSG:32637" minx="0" maxy="0" dimensions="2" maxz="0" miny="0" minz="0" maxx="0"/>
    <temporal>
      <period>
        <start></start>
        <end></end>
      </period>
    </temporal>
  </extent>
</qgis>
