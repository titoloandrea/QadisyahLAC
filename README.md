# QadisyahLAC

Slides for the Landscape Archaeology Conference held at Qadisyah University (6 March 2023) created using [quarto](https://quarto.org/) and [revealjs](https://revealjs.com/).

NB: GIS data used to produce figures are stored in a separate repository versioned in kart (not yet public).